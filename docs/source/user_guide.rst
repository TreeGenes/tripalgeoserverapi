Tripal Geoserver API User's Guide
=================================

.. toctree::
  :maxdepth: 1
  :caption: Table of Contents
  :glob:

  user_guide/what_is_tripalgeoserverapi
  user_guide/server_setup
  user_guide/install_tripal_geoserver
  user_guide/user_manual