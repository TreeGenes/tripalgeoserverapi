Setup and Configure
===================

.. toctree::
   :maxdepth: 1
   :caption: Table of Contents
   :glob:

   ./install_tripal_geoserver/easy_install
   ./install_tripal_geoserver/module_install
   ./install_tripal_geoserver/module_enable