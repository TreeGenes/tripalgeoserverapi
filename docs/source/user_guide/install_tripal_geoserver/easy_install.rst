*********************************
Tripal and Geoserver Easy Install
*********************************

Tripal Install
==============

Go to `Tripal Install <https://github.com/Lowe-Man/tripal-install>`_ and follow the README at the bottom of the repository for how to easily setup Tripal.

Geoserver Install
=================

Run git clone

.. code-block:: bash

   git clone https://github.com/Lowe-Man/geoserver_install

Make sure script is executable

.. code-block:: bash

   chmod u+x -R geoserver_install

Run Geoserver Install script

.. code-block:: bash

   cd geoserver_install
   ./run.sh