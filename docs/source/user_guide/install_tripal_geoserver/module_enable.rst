**************
Module Enable
**************

.. contents:: Table of Contents

Step 1 - Go to Modules Tab
==========================

.. image:: ../../_static/images/Module_Enable/Step_1.png

Step 2 - Find Module in List
============================

Search for ``tripalgeoserverapi`` in the list of modules and then click the checkbox. From there scroll to the bottom of the page and click ``Save configuration``

.. image:: ../../_static/images/Module_Enable/Step_2.png

.. admonition:: success
   :class: success

   Yayy!!! You have successfully enabled the module.

.. note:: If you are trying to find permissions go find the **tripalgeoserverapi** module in the list and click the permissions link.

