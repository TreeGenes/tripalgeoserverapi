**************
Module Install
**************

.. contents:: Table of Contents

.. note:: If you have not installed Tripal and Geoserver please do so by following :doc:`easy_install` tutorial.

Step 1 - Cloning Repository
===========================

.. note:: For the rest of this tutorial we are going to assume that you followed :doc:`easy_install` tutorial and that your drupal installation is at ``/var/www/drupal``.

To install the module you are going to go to navigate to ``/var/www/drupal/sites/all/modules/`` by running this command.

.. code-block:: bash

   cd /var/www/drupal/sites/all/modules/

In order to get the module are you going to run. This will download the module to the `modules` directory that we navigated to in the previous step.

.. code-block:: bash

   git clone https://gitlab.com/TreeGenes/tripalgeoserverapi.git

Step 2 - Installing Dependencies
================================

The next thing to do is to pull its dependencies down using `composer`.

.. code-block:: bash

   cd tripalgeoserverapi
   composer install

Final thing is to reload the module so that drupal can reread it.

.. code-block:: bash

   drush8 cc all


.. admonition:: success
   :class: success

   Yayy!!! You have successfully installed the tripalgeoserverapi module