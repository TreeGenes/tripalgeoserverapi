Server Setup
============

.. toctree::
   :maxdepth: 1
   :caption: Table of Contents
   :glob:

   ./server_setup/requirements
   ./server_setup/Ubuntu_20.04_LTS_Install