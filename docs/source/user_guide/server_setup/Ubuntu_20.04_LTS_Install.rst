Ubuntu 20.04 LTS Install
========================

.. toctree::
   :maxdepth: 1
   :caption: Table of Contents
   :glob:

   ./Ubuntu_20.04_LTS_install/create_iso_media
   ./Ubuntu_20.04_LTS_install/desktop