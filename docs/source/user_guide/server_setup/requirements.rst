Requirements
============

Before installation of Tripal Geoserer API module, a web server must be configured and ready, the following dependencies:

1. A UNIX-based server (e.g. Ubuntu Linux).
2. Web server software. The `Apache web server <https://httpd.apache.org/>`_ is most commonly used.
3. `PHP <http://php.net/>`_ version 5.6 or higher (the most recent version is recommended).
4. `PostgreSQL <https://www.postgresql.org>`_ 12 or higher
5. `Drush <http://www.drush.org/en/master/>`_ 7 or higher
6. `Drupal <https://www.drupal.org/>`_ 7.

.. warning::

  PHP 7.2 is not fully compatible with Drupal.