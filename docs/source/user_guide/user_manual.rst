User Manual
===========

.. toctree::
   :maxdepth: 1
   :caption: Table of Contents
   :glob:

   ./user_manual/admin
   ./user_manual/workspaces
   ./user_manual/coverage_store
   ./user_manual/data_store