***********
Admin Setup
***********

.. contents:: Table of Contents

Step 1 - Find "Geoserver Admin" Tab
===================================

.. image:: ../../_static/images/User_Manual/Admin/Admin_Tab.png

Step 2 - Add Credentials
========================

.. image:: ../../_static/images/User_Manual/Admin/Credential_Management-add.png

Fill out the form with the connection details relative to your setup and server.

.. image:: ../../_static/images/User_Manual/Admin/Add_GeoServer_Credentials.png

.. note:: If you want to have multiple credentials you can add as many as you want (ex development and production)

Step 3 - Set to Use
===================

.. image:: ../../_static/images/User_Manual/Admin/Credential_Management-use.png

Once "use" is clicked you will come to a message that looks like the one below asking you to confirm if you want to use these credentials to connect and get data from the GeoServer.

.. image:: ../../_static/images/User_Manual/Admin/Credential_Management-use-confirm.png
