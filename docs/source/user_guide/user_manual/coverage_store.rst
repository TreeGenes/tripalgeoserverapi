***********************
Coverage Store Overview
***********************

.. contents:: Table of Contents

What is it?
===========

Contains raster format spatial data.

.. image:: ../../_static/images/User_Manual/User/Coverage_Store/Overview.png

Add Coverage Store
==================

Click "Add Coverage Store" button within a Workspace.

.. image:: ../../_static/images/User_Manual/User/Coverage_Store/Add_Coverage_Store.png

A form will appear where you can enter the name of the Coverage Store and a description for it. The last part of the form is a "GeoTIFF Upload" box where a ".TIFF" file can be uploaded.

.. image:: ../../_static/images/User_Manual/User/Coverage_Store/Add_Coverage_Store_Form.png

Edit Coverage Store
===================

Click the "edit" button in the "Operations" column

.. image:: ../../_static/images/User_Manual/User/Coverage_Store/Edit_Coverage_Store.png

A form will appear where you can enter the description of the Coverage Store and enable or disable the Coverage Store by checking or unchecking the box respectively.

.. image:: ../../_static/images/User_Manual/User/Coverage_Store/Edit_Coverage_Store_Form.png

Delete Coverage Store
=====================

Click the "delete" button in the "Operations" column

.. image:: ../../_static/images/User_Manual/User/Coverage_Store/Delete_Coverage_Store.png

A delete confirmation form will appear. Make sure you are deleting the correct Coverage Store before clicking that "Delete" button.

.. image:: ../../_static/images/User_Manual/User/Coverage_Store/Delete_Coverage_Store_Confirmation.png
