*******************
Data Store Overview
*******************

.. contents:: Table of Contents

What is it?
===========

Contains vector format spatial data such as shapefiles

.. image:: ../../_static/images/User_Manual/User/Data_Store/Overview.png

Add Data Store
==============

Click "Add Data Store" button within a Workspace.

.. image:: ../../_static/images/User_Manual/User/Data_Store/Add_Data_Store.png

A form will appear where you can enter the name of the Data Store and a description for it. The last part of the form is a "Datastore Upload" box where a shapefile can be uploaded. `Here <https://docs.geoserver.org/stable/en/user/gettingstarted/shapefile-quickstart/index.html>`_ is an example of a shapefile.

.. image:: ../../_static/images/User_Manual/User/Data_Store/Add_Data_Store_Form.png

Edit Data Store
===============

Click the "edit" button in the "Operations" column

.. image:: ../../_static/images/User_Manual/User/Data_Store/Edit_Data_Store.png

A form will appear where you can enter the description of the Data Store and enable or disable the Data Store by checking or unchecking the box respectively.

.. image:: ../../_static/images/User_Manual/User/Data_Store/Edit_Data_Store_Form.png

Delete Data Store
=================

Click the "delete" button in the "Operations" column

.. image:: ../../_static/images/User_Manual/User/Data_Store/Delete_Data_Store.png

A delete confirmation form will appear. Make sure you are deleting the correct Data Store before clicking that "Delete" button.

.. image:: ../../_static/images/User_Manual/User/Data_Store/Delete_Data_Store_Confirmation.png
