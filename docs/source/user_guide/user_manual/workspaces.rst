*******************
Workspaces Overview
*******************

.. contents:: Table of Contents

What is it?
===========

A workspace is used to group similar layers together. Layers may be referred to by their workspace name

.. image:: ../../_static/images/User_Manual/User/Workspace/Overview.png

Add Workspace
=============

Click the "Add Workspace" button at the top of the page. A text box will appear where you can enter a name for your new Workspace.

.. image:: ../../_static/images/User_Manual/User/Workspace/Add_Workspace.png

Edit Workspace
==============

Click "edit" next to one of the workspaces. A text box will appear where you can change the name of the Workspace.

.. image:: ../../_static/images/User_Manual/User/Workspace/Edit_Workspace.png

Delete Workspace
================

Click "delete" next to one of the workspaces. A prompt will appear where you can delete the Workspace.

.. note:: Make sure to check "Recurse" in order to successfully delete all contents of the Workspace

.. image:: ../../_static/images/User_Manual/User/Workspace/Delete_Workspace.png