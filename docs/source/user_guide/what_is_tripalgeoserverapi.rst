What Is Tripal Geoserver?
=========================

This is a Drupal 7 module that allows anyone to interact with Geoserver
easily through simple forms. The module has been designed to work with
`Tripal <https://tripal.readthedocs.io/en/latest/user_guide/what_is_tripal.html>`__
but realist has no direct relationship to Tripal that prohibits its use
outside that ecosystem.
