<?php

function get_use_cred() {
  return db_query("SELECT geo.username, geo.password, geo.base_url FROM tripalgeoserverapi as geo JOIN tripalgeoserverapi_credential_use as cred ON geo.id = cred.id;")->fetchObject();
}

function renderPhpToString($file, $vars = NULL) {
  if (is_array($vars) && !empty($vars)) {
    extract($vars);
  }
  ob_start();
  include $file;
  return ob_get_clean();
}