<?php

use Lowem\GeoserverPHP\CoverageStores;
use Lowem\GeoserverPHP\DataStores;
use Lowem\GeoserverPHP\Workspaces;
use Lowem\EasyCurl\HTTPRequestException;

$modulePath = drupal_get_path("module", "tripalgeoserverapi");
require_once "$modulePath/includes/functions.php";
require_once "$modulePath/vendor/autoload.php";

/*
 * Workspace Form
 */

function tripalgeoserverapi_workspace_form($form, &$form_state) {
  $workspace_name = $form_state["build_info"]["args"][1] ?? "";

  if ($form_state["build_info"]["args"][0] == "add") {
    $form["workspace_name"] = [
      "#type" => "textfield",
      "#title" => "Workspace Name",
    ];

    $form["submit"] = [
      "#type" => "submit",
      "#name" => "add",
      "#value" => "Add"
    ];
  } elseif ($form_state["build_info"]["args"][0] == "edit") {
    $form["current_workspace_name"] = [
      "#type" => "hidden",
      "#value" => $workspace_name
    ];

    $form["new_workspace_name"] = [
      "#type" => "textfield",
      "#title" => "New Workspace Name",
    ];

    $form["submit"] = [
      "#type" => "submit",
      "#name" => "edit",
      "#value" => "Edit"
    ];
  } else {
    $table = [
      "header" => NULL,
      "rows" => [
        [
          [
            "data" => "Name",
            "header" => TRUE
          ],
          $workspace_name
        ]
      ],
    ];

    $form["info"] = [
      "#type" => "markup",
      "#markup" => "<p>Would you like to delete the following workspace?</p>" . theme("table", $table)
    ];

    $form["workspace_name"] = [
      "#type" => "hidden",
      "#value" => $workspace_name
    ];

    $form["recurse"] = [
      "#type" => "checkbox",
      "#title" => "Recurse",
    ];

    $form["delete"] = [
      "#type" => "submit",
      "#value" => "Delete",
      "#name" => "delete",
      "#suffix" => "<span> " . l("or Cancel", "tripalgeoserverapi/workspaces/get") . "</span>",
    ];
  }
  return $form;
}

function tripalgeoserverapi_workspace_form_submit($form, &$form_state) {
  $form_state["rebuild"] = FALSE;
  $form_state["redirect"] = "tripalgeoserverapi/workspaces/get";

  $userCreds = get_use_cred();

  $workspace = new Workspaces($userCreds->base_url);
  $workspace->setBasicAuth($userCreds->username, $userCreds->password);

  if ($form_state["triggering_element"]["#name"] == "add") {
    $workspaceName = $form_state["values"]["workspace_name"];

    try {
      $workspace->create($workspaceName);
      drupal_set_message("Successfully Added a Workspace");
    } catch (HTTPRequestException $e) {
      $error = $e->getCustomMessage();
      drupal_set_message($error, "error");
    }
  } elseif ($form_state["triggering_element"]["#name"] == "edit") {
    $currentWorkspaceName = $form_state["values"]["current_workspace_name"];
    $newWorkspaceName = $form_state["values"]["new_workspace_name"];

    try {
      $workspace->update($currentWorkspaceName, $newWorkspaceName);
      drupal_set_message("Successfully Updated a Workspace");
    } catch (HTTPRequestException $e) {
      $error = $e->getCustomMessage();
      drupal_set_message($error, "error");
    }
  } else {
    $workspaceName = $form_state["values"]["workspace_name"];
    $recurse = $form_state["values"]["recurse"] ?? "false";

    try {
      $workspace->delete($workspaceName, $recurse);
      drupal_set_message("Successfully Deleted a Workspace");
    } catch (HTTPRequestException $e) {
      $error = $e->getCustomMessage();
      drupal_set_message($error, "error");
    }
  }
}

/*
 * Get workspaces
 */

function tripalgeoserverapi_workspace_table() {
  $modulePath = drupal_get_path("module", "tripalgeoserverapi");
  $output = l("Add Workspace", "tripalgeoserverapi/workspace/add");
  $userCreds = get_use_cred();

  $workspace = new Workspaces($userCreds->base_url);
  $workspace->setBasicAuth($userCreds->username, $userCreds->password);
  try {
    $workspace_table = [
      "header" => [
        [
          "data" => "Name",
          "width" => "90%"
        ],
        [
          "data" => "Operations",
          "width" => "10%"
        ],
      ],
      "rows" => [],
    ];

    $workspaceJson = json_decode($workspace->getAll());
    $workspaces = $workspaceJson->workspaces->workspace;
    foreach ($workspaces as $workspace) {
      $workspaceLinks = l("edit", "tripalgeoserverapi/workspace/edit/" . urlencode($workspace->name)) . " | " . l("delete", "tripalgeoserverapi/workspace/delete/" . urlencode($workspace->name));

      $coverageStoreTable = [
        "header" => [
          [
            "data" => "Coverage Store Name",
            "width" => "70%"
          ],
          [
            "data" => "Operations",
            "width" => "30%"
          ],
        ],
        "rows" => [],
      ];


      $workspace_content = "Workspace Name: <b>$workspace->name</b><br/><br/>";
      $workspace_content .= l("Add Coverage Store", "tripalgeoserverapi/coveragestore/add/" . urlencode($workspace->name));

      $coverageStore = new CoverageStores($userCreds->base_url);
      $coverageStore->setBasicAuth($userCreds->username, $userCreds->password);

      $coverageStoreJson = json_decode($coverageStore->getAll($workspace->name));

      $coverageStores = [];
      if (!empty($coverageStoreJson->coverageStores)) {
        $coverageStores = $coverageStoreJson->coverageStores->coverageStore;
      } else {
        $coverageStoreTable["header"] = [
          "data" => "Coverage Store Name"
        ];
        $coverageStoreTable["rows"][] = [
          "<b>None</b>",
        ];
      }

      foreach ($coverageStores as $coverageStore) {
        $coverageStoreLinks = l("edit", "tripalgeoserverapi/coveragestore/edit/" . urlencode($workspace->name) . "/" . urlencode($coverageStore->name)) . " | " . l("delete", "tripalgeoserverapi/coveragestore/delete/" . urlencode($workspace->name) . "/" . urlencode($coverageStore->name));

        $coverageStoreTable["rows"][] = [
          "<b>$coverageStore->name</b>",
          $coverageStoreLinks
        ];
      }

      $workspace_content .= theme("table", $coverageStoreTable) . "<br>";

      $workspace_content .= l("Add Data Store", "tripalgeoserverapi/datastore/add/" . urlencode($workspace->name));

      $dataStore = new DataStores($userCreds->base_url);
      $dataStore->setBasicAuth($userCreds->username, $userCreds->password);

      $dataStoreJson = json_decode($dataStore->getAll($workspace->name));

      $dataStoreTable = [
        "header" => [
          [
            "data" => "Data Store Name",
            "width" => "70%"
          ],
          [
            "data" => "Operations",
            "width" => "30%"
          ],
        ],
        "rows" => [],
      ];

      $dataStores = [];
      if (!empty($dataStoreJson->dataStores)) {
        $dataStores = $dataStoreJson->dataStores->dataStore;
      } else {
        $dataStoreTable["header"] = [
          "data" => "Data Store Name"
        ];
        $dataStoreTable["rows"][] = [
          "<b>None</b>",
        ];
      }

      foreach ($dataStores as $dataStore) {
        $datastoreLinks = l("edit", "tripalgeoserverapi/datastore/edit/" . urlencode($workspace->name) . "/" . urlencode($dataStore->name)) . " | " . l("delete", "tripalgeoserverapi/datastore/delete/" . urlencode($workspace->name) . "/" . urlencode($dataStore->name));

        $dataStoreTable["rows"][] = [
          "<b>$dataStore->name</b>",
          $datastoreLinks
        ];
      }

      $workspace_content .= theme("table", $dataStoreTable) . "<br>";

      $workspace_table["rows"][] = [
        $workspace_content,
        $workspaceLinks
      ];
    }
    $output .= theme("table", $workspace_table);
    return $output;
  } catch (HTTPRequestException $e) {
    $error = $e->getCustomMessage();
    drupal_set_message($error, "error");
  }
  $output .= renderPhpToString("$modulePath/includes/partials/user/list.php");
  return $output;
}

function tripalgeoserverapi_coveragestore_form($form, &$form_state) {
  $workspaceName = $form_state["build_info"]["args"][1] ?? "";
  $coverageStoreName = $form_state["build_info"]["args"][2] ?? "";

  if ($form_state["build_info"]["args"][0] == "add") {
    $form["workspace_name"] = [
      "#type" => "hidden",
      "#value" => $workspaceName
    ];

    $form["coverage_store_name"] = [
      "#type" => "textfield",
      "#title" => "Coverage Store Name",
    ];

    $form["coverage_store_description"] = [
      "#type" => "textfield",
      "#title" => "Description",
    ];

    $form["geotiff_file"] = [
      "#type" => "file",
      "#title" => t("GeoTIFF Upload")
    ];

    $form["submit"] = [
      "#type" => "submit",
      "#name" => "add",
      "#value" => "Add"
    ];
  } elseif ($form_state["build_info"]["args"][0] == "edit") {
    $form["coverage_store_name"] = [
      "#type" => "hidden",
      "#value" => $coverageStoreName,
    ];

    $form["workspace_name"] = [
      "#type" => "hidden",
      "#value" => $workspaceName,
    ];

    $form["coverage_store_description"] = [
      "#type" => "textfield",
      "#title" => "Description",
    ];

    $form["enable"] = [
      "#type" => "checkbox",
      "#title" => "Enable",
    ];

    $form["submit"] = [
      "#type" => "submit",
      "#name" => "edit",
      "#value" => "Edit"
    ];
  } elseif ($form_state["build_info"]["args"][0] == "delete") {
    $table = [
      "header" => NULL,
      "rows" => [
        [
          [
            "data" => "Name",
            "header" => TRUE
          ],
          $coverageStoreName
        ]
      ],
    ];

    $form["info"] = [
      "#type" => "markup",
      "#markup" => "<p>Would you like to delete the following Coverage Store?</p>" . theme("table", $table)
    ];

    $form["coverage_store_name"] = [
      "#type" => "hidden",
      "#value" => $coverageStoreName,
    ];

    $form["workspace_name"] = [
      "#type" => "hidden",
      "#value" => $workspaceName,
    ];

    $form["delete"] = [
      "#type" => "submit",
      "#value" => "Delete",
      "#name" => "delete",
      "#suffix" => "<span> " . l("or Cancel", "tripalgeoserverapi/workspaces/get") . "</span>",
    ];
  }
  return $form;
}

function tripalgeoserverapi_coveragestore_form_submit($form, &$form_state) {
  $form_state["rebuild"] = FALSE;
  $form_state["redirect"] = "tripalgeoserverapi/workspaces/get";

  $userCreds = get_use_cred();

  $coverageStore = new CoverageStores($userCreds->base_url);
  $coverageStore->setBasicAuth($userCreds->username, $userCreds->password);

  $workspaceName = $form_state["values"]["workspace_name"];
  $coverageStoreName = $form_state["values"]["coverage_store_name"];

  if ($form_state["triggering_element"]["#name"] == "add") {
    $coverageStoreDescription = $form_state["values"]["coverage_store_description"];

    $validators = [
      "file_validate_extensions" => ["tif"]
    ];

    $location = "public://tripalgeoserverapi";

    $fileUpload = file_save_upload("geotiff_file", $validators, $location);
    $file = file_load($fileUpload->fid);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);

    $file_path = drupal_realpath($file->uri);

    try {
      $coverageStore->create([
        "name" => $coverageStoreName,
        "workspace" => $workspaceName,
        "description" => $coverageStoreDescription,
        "enabled" => "false",
        "type" => "GeoTIFF"
      ]);
      $coverageStore->geoTiffUpload($workspaceName, $coverageStoreName, $file_path);
      $coverageStore->update($workspaceName, $coverageStoreName, [
        "enabled" => "true"
      ]);
      drupal_set_message("Successfully Added a Coverage Store");
      file_delete($file);
    } catch (HTTPRequestException $e) {
      $error = $e->getCustomMessage();
      drupal_set_message($error, "error");
    }
  } elseif ($form_state["triggering_element"]["#name"] == "edit") {
    $workspaceDescription = $form_state["values"]["description"];
    $enable = $form_state["values"]["enable"];

    try {
      $coverageStore->update($workspaceName, $coverageStoreName, [
        "name" => $coverageStoreName,
        "description" => $workspaceDescription,
        "workspace" => $workspaceName,
        "enabled" => $enable,
        "type" => "GeoTIFF"
      ]);
    } catch (HTTPRequestException $e) {
      echo $e->getCustomMessage();
    }
  } elseif ($form_state["triggering_element"]["#name"] == "delete") {
    try {
      $coverageStore->delete($workspaceName, $coverageStoreName);
    } catch (HTTPRequestException $e) {
      echo $e->getCustomMessage();
    }
  }
}

function tripalgeoserverapi_datastore_form($form, &$form_state) {
  $workspaceName = $form_state["build_info"]["args"][1] ?? "";
  $dataStoreName = $form_state["build_info"]["args"][2] ?? "";

  if ($form_state["build_info"]["args"][0] == "add") {
    $form["workspace_name"] = [
      "#type" => "hidden",
      "#value" => $workspaceName
    ];

    $form["data_store_name"] = [
      "#type" => "textfield",
      "#title" => "Data Store Name",
    ];

    $form["data_store_description"] = [
      "#type" => "textfield",
      "#title" => "Description",
    ];

    $form["data_store_file"] = [
      "#type" => "file",
      "#title" => t("Datastore Upload")
    ];

    $form["submit"] = [
      "#type" => "submit",
      "#name" => "add",
      "#value" => "Add"
    ];
  } elseif ($form_state["build_info"]["args"][0] == "edit") {
    $form["datastore_name"] = [
      "#type" => "hidden",
      "#value" => $dataStoreName,
    ];

    $form["workspace_name"] = [
      "#type" => "hidden",
      "#value" => $workspaceName,
    ];

    $form["data_store_description"] = [
      "#type" => "textfield",
      "#title" => "Description",
    ];

    $form["enable"] = [
      "#type" => "checkbox",
      "#title" => "Enable",
    ];

    $form["submit"] = [
      "#type" => "submit",
      "#name" => "edit",
      "#value" => "Edit"
    ];
  } elseif ($form_state["build_info"]["args"][0] == "delete") {
    $table = [
      "header" => NULL,
      "rows" => [
        [
          [
            "data" => "Name",
            "header" => TRUE
          ],
          $dataStoreName
        ]
      ],
    ];

    $form["info"] = [
      "#type" => "markup",
      "#markup" => "<p>Would you like to delete the following Data Store?</p>" . theme("table", $table)
    ];

    $form["data_store_name"] = [
      "#type" => "hidden",
      "#value" => $dataStoreName,
    ];

    $form["workspace_name"] = [
      "#type" => "hidden",
      "#value" => $workspaceName,
    ];

    $form["delete"] = [
      "#type" => "submit",
      "#value" => "Delete",
      "#name" => "delete",
      "#suffix" => "<span> " . l("or Cancel", "tripalgeoserverapi/workspaces/get") . "</span>",
    ];
  }
  return $form;
}

function tripalgeoserverapi_datastore_form_submit($form, &$form_state) {
  $form_state["rebuild"] = FALSE;
  $form_state["redirect"] = "tripalgeoserverapi/workspaces/get";

  $userCreds = get_use_cred();

  $datastore = new DataStores($userCreds->base_url);
  $datastore->setBasicAuth($userCreds->username, $userCreds->password);

  $workspaceName = $form_state["values"]["workspace_name"];
  $dataStoreName = $form_state["values"]["data_store_name"];

  if ($form_state["triggering_element"]["#name"] == "add") {
   $dataStoreDescription = $form_state["values"]["data_store_description"];

    $validators = [
      "file_validate_extensions" => ["zip"]
    ];

    $location = "public://tripalgeoserverapi";

    $file_upload = file_save_upload("data_store_file", $validators, $location);
    $file = file_load($file_upload->fid);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);

    $file_path = drupal_realpath($file->uri);

    try {
      $datastore->shapeFileUpload($workspaceName, $dataStoreName, $file_path);
      $datastore->update($workspaceName, $dataStoreName, [
        "enabled" => "true",
        "description" => $dataStoreDescription
      ]);
      drupal_set_message("Successfully Created a Data Store");
      file_delete($file);
    } catch (HTTPRequestException $e) {
      $error = $e->getCustomMessage();
      drupal_set_message($error, "error");
    }
  } elseif ($form_state["triggering_element"]["#name"] == "edit") {
    $dataStoreDescription = $form_state["values"]["data_store_description"];
    $enable = $form_state["values"]["enable"];

    try {
      $datastore->update($workspaceName, $dataStoreName, [
        "name" => $dataStoreName,
        "description" => $dataStoreDescription,
        "workspace" => $workspaceName,
        "enabled" => $enable,
      ]);
    } catch (HTTPRequestException $e) {
      echo $e->getCustomMessage();
    }
  } elseif ($form_state["triggering_element"]["#name"] == "delete") {
    try {
      $datastore->delete($workspaceName, $dataStoreName);
    } catch (HTTPRequestException $e) {
      echo $e->getCustomMessage();
    }
  }
}